# mvp-browser

A tiny browser in Ruby, using GTK and WebKit.

## Screenshots

![Screenshot.](https://gitlab.com/duckinator/ruby-mvp-browser/raw/main/screenshot.png)
